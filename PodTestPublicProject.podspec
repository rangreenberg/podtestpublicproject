Pod::Spec.new do |s|
  s.name         = "PodTestPublicProject"
  s.version      = "1.0"
  s.summary      = "Pod Test Public Project"
  s.homepage         = "http://google.com"
  s.license      = { :type => 'Apache License, Version 2.0', :text => <<-LICENSE
      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
      LICENSE
    }
  s.author       = { "Ran Greenberg" => "ran.greenberg@mako.co.il" }
  s.source       = { 
    :git => "https://rangreenberg@bitbucket.org/rangreenberg/podtestpublicproject.git",
	:tag => 'v1.0'
  }

  s.platform     = :ios, '7.0'
  s.source_files = 'PodTestPublicProject/*.{h,m}'
  s.requires_arc = true
  
end
