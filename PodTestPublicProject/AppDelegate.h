//
//  AppDelegate.h
//  PodTestPublicProject
//
//  Created by Ran Greenberg on 6/22/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
